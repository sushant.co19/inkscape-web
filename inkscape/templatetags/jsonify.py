#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
#
"""
Simple python to json converter
"""
import types
import json

from decimal import Decimal
from datetime import timedelta

from django import template
from django.utils.safestring import mark_safe
from django.core.serializers.json import DjangoJSONEncoder

register = template.Library()

class DjangoEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, timedelta):
            ARGS = ('days', 'seconds', 'microseconds')
            return {'__type__': 'datetime.timedelta',
                    'args': [getattr(obj, a) for a in ARGS]}
        # Decimals are output as a string by default, which Chrome can't handle
        elif isinstance(obj, Decimal):
            return float(obj)
        else:
            return DjangoJSONEncoder.default(self, obj)

@register.filter
def jsonify(obj):
    if isinstance(obj, types.GeneratorType):
        obj = list(obj)
    return mark_safe(json.dumps(obj, cls=DjangoEncoder))

