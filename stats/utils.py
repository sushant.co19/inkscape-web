#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=too-many-ancestors
#
"""Utlities for the stats package"""

import io
import os
import gzip
import time
import json
import socket
import struct
import requests

from datetime import date, datetime, timedelta

from django.conf import settings

class InvalidData(ValueError):
    """Used when the statistical data is invalid"""

class NoData(ValueError):
    """No data was available"""

def get_stats(request, mod, **options):
    """Return a list of statistics for this module"""
    from .base import StatisticBase, Statistics
    options = options
    options.update(request.GET)
    return Statistics([cls(options) for cls in StatisticBase.STATS[mod]])

def doy(date):
    """Return day of year from datetime"""
    return (date - datetime(date.year, 1, 1, tzinfo=date.tzinfo)).days + 1

class Week(date):
    """Convert to the week (monday)"""
    def __new__(cls, dtm):
        if isinstance(dtm, datetime):
            dtm = dtm.date()
        dtm -= timedelta(days=dtm.weekday())
        return super().__new__(cls, dtm.year, dtm.month, dtm.day)

    def __str__(self):
        (year, week, day) = self.isocalendar()
        return f"{year}-w{week}"

def file_age(filename):
    """Returns the number of days old"""
    seconds = time.time() - os.stat(filename).st_mtime
    return int(seconds / 60 / 60 / 24)

class Network(object):
    """Faster comparisons of network addressess"""
    _long = staticmethod(lambda chars: struct.unpack("!L", chars)[0])
    _ip = classmethod(lambda cls, addr: cls._long(socket.inet_aton(addr)))

    def __init__(self, network):
        self.network = network
        broadcast, mask = network.split('/')
        self.addr = self._ip(broadcast)
        self.mask = self._long(struct.pack('!I', (1 << 32) - (1 << (32 - int(mask)))))

    def __str__(self):
        return self.network

    def __contains__(self, other):
        try:
            return (self._ip(other) & self.mask) == self.addr
        except OSError:
            return False

class FastlyNetworks(object):
    cache = []
    def __init__(self):
        if not self.cache:
            self.populate()

    def __iter__(self):
        return self.cache.__iter__()

    @classmethod
    def populate(cls, url='https://api.fastly.com/public-ip-list', age=14):
        fast_file = os.path.join(settings.LOGS_ROOT, 'fastly.ips')

        if os.path.isfile(fast_file) and file_age(fast_file) < age:
            with open(fast_file, 'r') as fhl:
                result = json.loads(fhl.read())
        else:
            resp = requests.get(url)
            if resp.status_code != 200:
                if resp.status_code == 404:
                    raise RemoteError("File Not Found")
                raise RemoteError("Server Error")

            result = json.loads(resp.content)
            with open(fast_file, 'w') as fhl:
                fhl.write(json.dumps(result))
        cls.cache = [Network(addr) for addr in result['addresses']]

class LogOpen(object):
    """Open up a log, gzip or plain text and parse using regex"""
    def __init__(self, filename, rex, reset=False, bad=False):
        self.fhl = None
        self.rex = rex
        self.tell = 0
        self.count = 0
        self.filename = filename
        self.record = filename + '.book'
        self.size = int(os.stat(filename)[6])
        self.inode = os.stat(filename)[1]
        self.reset = reset
        self.bad = bad

    def __enter__(self):
        seek = 0 if self.reset else self.load_record()
        self.fhl = open(self.filename, 'rb')
        # GZip magic number for svgz files.
        if self.peek(2) == b'\x1f\x8b':
            # File handle is replaced with gzip io handle
            self.fhl = gzip.GzipFile(fileobj=self.fhl)
            # The internal size of a gzip is available
            self.size = self.fhl.seek(0, io.SEEK_END)
            self.fhl.seek(0)

        if seek and self.fhl.seekable():
            self.fhl.seek(seek)
        self.tell = self.fhl.tell()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.save_record(inode=self.inode, seek=self.tell)
        self.fhl.close()

    def load_record(self):
        """Loads the record file (if any) and returns the seek"""
        try:
            with open(self.record, 'r') as fhl:
                record = json.loads(fhl.read())
            # inode matching ensures log-rotation protection
            if record['inode'] == self.inode:
                return record['seek']
        except (KeyError, ValueError, IOError):
            pass
        return 0

    def save_record(self, **data):
        """Save a record of the position in the log file"""
        with open(self.record, 'w') as fhl:
            fhl.write(json.dumps(data))

    def peek(self, size):
        """Read some bytes in the file then seek back"""
        pos = self.fhl.tell()
        ret = self.fhl.read(size)
        self.fhl.seek(pos)
        return ret

    def __iter__(self):
        if not self.fhl:
            raise IOError("File not opened yet.")

        while self.tell < self.size:
            self.count += 1
            line = self.fhl.readline().decode('utf8')
            match = self.rex.match(line.strip())
            if match and not self.bad:
                yield match.groupdict()
            elif not match and self.bad:
                yield line.strip()
            self.tell = self.fhl.tell()

def clean_numbers(arr):
    """Cleaned array of numbers, always at least one value (0)"""
    return [0] + [a for a in arr if isinstance(a, (int, float))]
