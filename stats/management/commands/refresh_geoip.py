#
# Copyright 2016, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Django command for processing log files.
"""

#
# This will not work with django > 1.9 as GeoIP2 is now used
# which requires both binary formatted geoip files as well as
# those files being locked away behind an API
#

import os
import sys

from io import StringIO
from gzip import GzipFile
from urllib.request import urlopen

from django.core.management.base import BaseCommand
from django.conf import settings

class Command(BaseCommand):
    """Command"""
    help = "Download a fresh copy of th GeoIP City and Country databases"

    def wget(self, url, save_to=settings.GEOIP_PATH):
        response = urlopen(url)
        if not os.path.isdir(save_to):
            os.makedirs(save_to)

        dest = os.path.join(save_to, os.path.basename(url))
        if url.endswith('.gz'):
            response = GzipFile(fileobj=StringIO(response.read()))
            dest = dest[:-3]

        with open(dest, 'wb') as output:
            output.write(response.read())

    def handle(self, **options):
        self.wget('http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz')
        self.wget('http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz')

