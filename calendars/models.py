#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Calendars for teams
"""

from datetime import timedelta
from django.utils.translation import ugettext_lazy as _
from django.db.models import (
    Model, Manager, QuerySet, TextField, CharField, URLField, BooleanField,
    DateTimeField, ForeignKey
)
from django.utils.timezone import now
from django.dispatch import receiver

from django.urls import reverse
from django.core.validators import MaxLengthValidator
from recurrence.fields import RecurrenceField

from person.models import Team, User

class EventQuerySet(QuerySet):
    breadcrumb_name = lambda self: _('Calendar')
    def breadcrumb_parent(self):
        return self._hints.get('instance', None)

    def get_absolute_url(self):
        team = self.breadcrumb_parent()
        if not team:
            return reverse('calendars:full')
        return reverse('team_calendar', kwargs={'team': team.slug})

    def get_ical_url(self):
        team = self.breadcrumb_parent()
        if not team:
            return reverse('calendars:full_feed')
        return reverse('team_calendar_feed', kwargs={'team': team.slug})

    def future_events(self):
        """Return just the future events"""
        return sorted([event for event in self if event > now()])

    def past_events(self):
        """Return just the past events"""
        return sorted([event for event in self if event < now()])


class Event(Model):
    """A single event"""
    team = ForeignKey(Team, related_name='events')
    creator = ForeignKey(User, related_name='created_team_events')

    title = CharField(max_length=255)
    description = TextField(validators=[MaxLengthValidator(4096)])
    recurrences = RecurrenceField(null=True, blank=True)
    start = DateTimeField()
    end = DateTimeField(null=True, blank=True)

    link = URLField(null=True, blank=True)
    is_video_link = BooleanField(default=True)

    objects = EventQuerySet.as_manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('calendars:event', kwargs={'pk': self.pk})

    def breadcrumb_parent(self):
        return self.team.events

    def include_rules_text(self):
        for rule in self.recurrences.rrules:
            yield rule.to_text()

    def exlcude_rules_text(self):
        for rule in self.recurrences.exrules:
            yield rule.to_text()

    def _dtm(self):
        return self.next_end if self.end else self.next_start

    def __lt__(self, other):
        return self._dtm() < other

    def __gt__(self, other):
        return self._dtm() > other

    @property
    def repeat(self):
        return Repeat(self.recurrences, self.start, self.end,
            exceptions=self.exceptions.all().flat_list())

    @property
    def next_start(self):
        """Next start datetime"""
        if self.recurrences:
            return self.repeat.next_start()
        return self.start

    @property
    def next_end(self):
        """Next end datetime"""
        if self.recurrences:
            dt = self.repeat.next_end()
            return self.repeat.next_end()
        return self.end

class ExceptionQuerySet(QuerySet):
    """Manage Exceptions"""
    def flat_list(self):
        for item in self:
            yield (item.old_start, item.new_start)
            if item.old_end:
                yield (item.old_end, item.new_end)

class EventException(Model):
    """A single exception to a regular event"""
    event = ForeignKey(Event, related_name='exceptions')

    old_start = DateTimeField()
    new_start = DateTimeField(null=True, blank=True)
    old_end = DateTimeField(null=True, blank=True)
    new_end = DateTimeField(null=True, blank=True)

    reason = CharField(max_length=128, null=True, blank=True)

    objects = ExceptionQuerySet.as_manager()

    def __str__(self):
        return 'Event moved...'

class Repeat(object):
    """Make recurrances easier to handle"""
    def __init__(self, rec, start, end, ref=None, exceptions=None):
        self.exceptions = dict(exceptions or {})
        if ref is None:
            ref = now()
        self.rec = rec
        self.start = start
        self.end = end
        self.ref = ref

    def _ex(self, dt):
        return self.exceptions.get(dt, dt)

    def _rec(self, ref):
        return Repeat(self.rec, self.start, self.end, ref, self.exceptions)

    def next_start(self):
        return self._ex(self.orig_next_start())

    def orig_next_start(self):
        return self.rec.after(self.ref, inc=True, dtstart=self.start)

    def next_end(self):
        return self._ex(self.orig_next_end())

    def orig_next_end(self):
        return self.rec.after(self.ref, inc=True, dtstart=self.end)

    def prev_start(self):
        return self._ex(self.rec.before(self.ref, inc=True, dtstart=self.start))

    def prev_end(self):
        return self._ex(self.rec.before(self.ref, inc=True, dtstart=self.end))

    def next_five(self):
        dt = self.orig_next_end()
        yield self
        for i in range(5):
            if dt is not None:
                rep = self._rec(dt + timedelta(days=1))
                dt = rep.orig_next_end()
                yield rep
