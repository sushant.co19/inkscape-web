#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
User utility functions
"""

import hashlib
import urllib

from resources.utils import url_filefield, RemoteError

def find_user_photo(user):
    """Find a user photo/avatar online"""
    if not user.photo:
        set_gravitar(user)
    return user

def set_gravitar(user):
    """Attempts to set the user photo to the user's gravitar"""
    if user.email:
        emh = hashlib.md5(user.email.lower().encode('utf-8')).hexdigest()
        url = f"https://www.gravatar.com/avatar/{emh}?s=190&d=404"
        try:
            user.photo = url_filefield(url, f"gravitar_{user.username}.png")
            if user.photo:
                user.save()
                return True
        except RemoteError:
            return False
    return False
